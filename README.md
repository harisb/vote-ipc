# TUBES SISTER - Voting System dengan IPC
Members :
Adlu Fadilah,
Aniq Wafa,
Haris Bayhaqi,
Ihsanuddienullah.

## Deskripsi
Proyek adalah sistem voting yang terdiri dari 4 node yaitu: 1 server, 2 client, dan 1 controller. Fungsi masing - masing node adalah sebagai berikut:
1. Server : menerima setiap vote yang dikirimkan client.
2. Client : mengirimkan vote ke server.
3. Controller: mengirimkan pesan ke server untuk memberhentikan penerimaan vote, kemudian menampilkan hasil vote.
## Tujuan
Proyek ini bertujuan untuk mendemonstrasikan pengertian anggota tentang sistem terdistribusi.

## COTS-Adlu-1301164127
Fitur yang ditambahkan adalah server bisa mengupload hasil pemenang voting yang nantinya bisa di download oleh client. Node yang ditambah dan dimodifikasi adalah:
1. Server: membuat file yang nantinya akan di upload
2. Client: mendownload file yang sudah di upload

## COTS-Muhammad Aniq Wafa' - 1301164405
Fitur yang ditambahkan pada proyek voting-IPC adalah kemampuan clientserver untuk mengkalkulasikan hasil pemungutan suara dalam bentuk diagram lingkaran serta menentukan pemenangnya dan akan dikirm ke server siapakah pemenang dari pemungutan suara.
node yang ditambahkan dan dimodifikasi adalah : 
1. Clientmaster : menambahkan vitur untuk menampilkan diagram lingkaran dan fungsi untuk menentukan pemenang. clientmaster ini juga berfungsi sebagai controller
2. Server : menerima pesan pemenang dari pemungutan suara dan menampilkannya

dalam file saya terdapat 3 file yang bisa dijalankan untuk mensimulasikannya

## COTS-Haris-1301164040
Fitur yang ditambahkan pada proyek voting-IPC adalah kemampuan untuk mengirimkan hasil voting ke multiple client. Berikut adalah node yang ditambahkan dan dimodifikasi pada proyek
1. Server: menerima vote yang dikirimkan client, kemudian mengirimkan hasil voting ke clients
2. Client-Result: menampilkan hasil voting yang dikirimkan server

## COTS-Ihsanuddienullah-1301164487
Fitur yang ditambahkan pada proyek voting-IPC adalah kemampuan mengirimkan file hasil voting ke server menggunakan metode IPC ibaratkan form C1 hasil pemilu dikirimkan ke KPU.
node yang ditambahkan dan dimodifikasi adalah : 
1. Clientmaster : merubah hasil voting menjadi format .txt agar mudah dikirim ke server
2. Upload file : mengirim file .txt hasil voting ke sever
3. Server upload : menerima dan menyimpan hasil kiriman

*Untuk memudahkan pembacaan file code, dilakukan perubahan struktur code pada semua file menjadi Object Oriented*


