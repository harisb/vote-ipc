import socket
class server:
    def __init__(self):    
        self.candidate = [
            {'name': 'god',
             'votes': 0
                },
            {'name': 'satan',
             'votes': 0}
        ]
        self.port = 12345
        self.buffer_size = 8192
        self.server_vote = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_result = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connection_list = []
        print("Server is running")
    def set_connections(self):
        ip_source_vote = '127.0.0.2'
        self.server_vote.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server_vote.bind((ip_source_vote, self.port))
        self.server_vote.listen(5)
        ip_source_result = '127.0.0.3'
        self.server_result.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server_result.bind((ip_source_result, self.port))
        self.server_result.listen(5)
        conn, addr = self.server_result.accept()
        self.connection_list.append(conn)
        print("Server is Connected to Node with address = ", addr)
    def connect(self):
        data = ''
        while (data != 'stop'):
            conn, addr = self.server_vote.accept()
            print("Server is Connected to Node with address = ", addr)
            data = conn.recv(self.buffer_size).decode()
            if(data == self.candidate[0]['name']):
                self.candidate[0]['votes'] += 1
                conn.send('accepted'.encode())
            elif(data == self.candidate[1]['name']):
                self.candidate[1]['votes'] += 1
                conn.send('accepted'.encode())
            else:
                conn.send(str(self.candidate).encode())
        conn.close()
    def send_results(self):
        for x in self.connection_list:
            try:
                x.send(str(self.candidate).encode())
            except:
                print("result send failed")
if __name__ == '__main__':
    test = server()
    test.set_connections()
    test.connect()
    test.send_results()
