import socket, json
import matplotlib.pyplot as plt

ip_dest = '127.0.0.1'
port = 12345
buffer_size = 8192
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect((ip_dest,port))
message = 'stop'.encode('utf-8')
client.send(message)
data = client.recv(buffer_size).decode()
data = data.replace("'", "\"")
data = json.loads(data)
labels = ['God', 'Satan']
datas = [data[0]['votes'], data[1]['votes']]
colors = ['lightskyblue', 'lightcoral']
explode = (0.01, 0)
print("God: ", data[0]['votes'])
print("Satan: ", data[1]['votes'])
plt.pie(datas, explode=explode, labels=labels, colors=colors, autopct='%1.1f%%', shadow=True, startangle=140)
plt.axis('equal')
plt.show()
if (data[0]['votes'] > data[1]['votes']):
	hasil = data[0]['name'].encode('utf-8')
elif (data[0]['votes'] < data[1]['votes']):
	hasil = data[0]['name'].encode('utf-8')
else:
	hasil = 'seri'.encode('utf-8')
client.send(hasil)
client.close()
