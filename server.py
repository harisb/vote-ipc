import socket
candidate = [
        {'name': 'god',
         'votes': 0
            },
        {'name': 'satan',
         'votes': 0}
    ]
ip_source = '127.0.0.1'
port = 12345
buffer_size = 8192
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind((ip_source,port))
data = ''
while (data != 'stop'):
    server.listen(5)
    conn, addr = server.accept()
    data = conn.recv(buffer_size).decode()
    if(data == candidate[0]['name']):
        candidate[0]['votes'] += 1
        conn.send('accepted'.encode())
    elif(data == candidate[1]['name']):
        candidate[1]['votes'] += 1
        conn.send('accepted'.encode())
    else:
        conn.send(str(candidate).encode())
conn.close()
