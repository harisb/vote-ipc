import socket, random
candidate = ['god', 'satan']
ip_dest = '127.0.0.1'
port = 12345
buffer_size = 8192
try:
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect((ip_dest,port))
    print("Candidate 1 = ", candidate[0])
    print("Candidate 2 = ", candidate[1])
    in_number = 0
    while((in_number > len(candidate)) or (in_number < 1)):
        in_number = int(input("Please enter the candidate number to vote: "))
    message = candidate[in_number-1].encode('utf-8')
    client.send(message)
    data = client.recv(buffer_size).decode()
    client.close()
    print("Vote Accepted")
except:
    print("Polling stopped")

