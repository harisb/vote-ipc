import socket, random
class client:
    def __init__(self):    
        self.candidate = ['god', 'satan']
        self.ip_dest = '127.0.0.2'
        self.port = 12345
        self.buffer_size = 8192
    def vote(self):        
        try:
            client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            client.connect((self.ip_dest,self.port))
            print("Candidate 1 = ", self.candidate[0])
            print("Candidate 2 = ", self.candidate[1])
            in_number = 0
            while((in_number > len(self.candidate)) or (in_number < 1)):
                in_number = int(input("Please enter the candidate number to vote: "))
            message = (self.candidate[in_number-1]).encode('utf-8')
            client.send(message)
            data = client.recv(self.buffer_size).decode()
            client.close()
            print('\n' * 40)
            print("Vote Accepted")
        except:
            print("Polling stopped")
            
if __name__ == '__main__':
    test = client()
    test.vote()
