import socket, json
class master:
    def __init__(self):
        self.ip_dest = '127.0.0.3'
        self.port = 12345
        self.buffer_size = 8192
    def receive_result(self):
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect((self.ip_dest, self.port))
        print("Node is Connected to server at IP = ", self.ip_dest)
        data = client.recv(self.buffer_size).decode()
        data = data.replace("'", "\"")
        data = json.loads(data)
        print("God: ", data[0]['votes'])
        print("Satan: ", data[1]['votes'])
        client.close()

if __name__ == '__main__':
    test = master()
    test.receive_result()
