import socket, random
candidate = ['god', 'satan']
ip_dest = '127.0.0.1'
port = 12345
buffer_size = 8192
data = ''
state = 'initial'
try:
    while data != 'stop':
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect((ip_dest,port))
        message = random.choice(candidate).encode('utf-8')
        client.send(message)
        data = client.recv(buffer_size).decode()
        if(state == 'initial'):
            print(data)
            state = 'normal'
        if(data == 'stop'):
            print(data)
        client.close()
except:
    print("Polling stopped")
